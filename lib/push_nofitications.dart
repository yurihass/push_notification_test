import 'package:aws_client/aws_client.dart';
import 'package:aws_client/s3.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http_client/console.dart';

class PushNotificationManager {
  PushNotificationManager._();

  factory PushNotificationManager() => _instance;

  static final PushNotificationManager _instance = PushNotificationManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  Future<void> init() async {
    if (!_initialized) {
      _firebaseMessaging.requestNotificationPermissions();
      _firebaseMessaging.configure();

      String token = await _firebaseMessaging.getToken();
      print("FirebaseMessaging token: $token");
      final region = 'sa-east-1';
      final appArn = 'Substituir...';
      final httpClient = ConsoleClient();
      final credentials =
          Credentials(accessKey: 'Substituir...', secretKey: 'Substituir...');

      final aws = Aws(credentials: credentials, httpClient: httpClient);
      final sns = aws.sns(region);
      print('appArn: $appArn');
      try {
        final endpoint =
            await sns.createEndpoint(applicationArn: appArn, pushToken: token);
        print('endpoint: ${endpoint.arn}');
      } catch (e) {
        print(e);
      }

      _initialized = true;
      print('object');
    }
  }
}
